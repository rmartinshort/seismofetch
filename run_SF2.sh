#!/bin/bash
#Script that runs the seismofetch program
seismodir="/home/robertms/Berkeley/SeismoFetchV2.0"
echo '

Running SeismoFetch program. Useful information about the progress of your request will appear in the terminal

'
cd $seismodir
python GUI_layout.py