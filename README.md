# README #

### What is this repository for? ###

SeismoFetch version 2.0: A simple GUI that allows users to download earthquake data (seismic traces) in bulk for a range of network/station configurations. When run with GMT-5, it also allows users to generate maps of their desired station/event combinations. Downloads data in SAC format and appends all sorts of useful information to the SAC header. 

### How do I get set up? ###

Works on Ubuntu Linux and Mac OSX: Simply clone the repository 

git clone https://rmartinshort@bitbucket.org/rmartinshort/seismofetch.git

And run 'INSTALL.py' 

### Who do I talk to? ###

Send questions to martinshortr@gmail.com